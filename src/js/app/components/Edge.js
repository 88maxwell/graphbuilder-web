import {cont} from "./../Consts";

export default class Edge {
    constructor(props) {
        const {v1, v2, width, color, weight} = props;

        this.v1 = v1;
        this.v2 = v2;
        this.width = width;
        this.color = color;
        this.weight = weight;
        
        this.draw();
    }

    draw() {
        cont.beginPath();
        cont.moveTo(this.v1.x, this.v1.y);
        cont.lineTo(this.v2.x, this.v2.y);
        cont.stroke();
    }

    is(x, y) {
        const a = (x - this.v1.x) / (this.v2.x - this.v1.x);
        const b = (y - this.v1.y) / (this.v2.y - this.v1.y);
        return a == b ? true : false;
    }
}
