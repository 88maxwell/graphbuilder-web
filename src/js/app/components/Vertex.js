import { cont } from "./../Consts";

export default class Vertex {
    constructor(props) {
        const {data, x, y, r} = props;
        this.data = data;
        this.x = x;
        this.y = y;
        this.r = r;

        this.state = false;

        this.draw();
    }

    draw() {
        
        cont.beginPath();
        if (this.state) {
            cont.fillStyle = "#234714";
        } else {
            cont.fillStyle = "#51be27";
        }
        cont.arc(this.x, this.y, this.r, 0, 2 * Math.PI);
        cont.stroke();
        cont.fill();
        cont.font = `${Math.pow(2, 1 / 2) * this.r}px Georgia`;
        cont.fillStyle = "#000000";
        cont.fillText(this.data, this.x - this.r, this.y + this.r / 2);
    }

    is(x, y) {
        return (Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2) <= Math.pow(this.r, 2)) ? true : false;
    }

    changePosition(x, y) {
        this.x = x;
        this.y = y;
    }

    setState(state) {
        this.state = state;
        this.draw();
    }
}
