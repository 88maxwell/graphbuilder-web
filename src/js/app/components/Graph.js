import { cont } from "./../Consts";
import * as Conf from "./../Configs";
import Vertex from "./Vertex";
import Edge from "./Edge";

export default class Graph {
    constructor() {
        this.vertexList = [];
        this.edgeList = [];
        this.chosenVertex = null;
        this.movingVertex = null;
        this.mouseDown = false;

        this.draw();
    }

    // ----------------------------------------------------
    // ------------ DRAW/REDRAW ELEMENTS
    // ----------------------------------------------------
    draw() {
        const { width, height, color } = Conf.Graph;
        cont.beginPath();
        cont.fillStyle = color;
        cont.fillRect(0, 0, width, height);
        cont.fill();
    }

    redrawVertex() {
        this.vertexList.forEach(vertex => vertex.draw());
    }

    redrawEdge() {
        this.edgeList.forEach(edge => edge.draw());
    }

    redrawAll() {
        this.draw();
        this.redrawEdge();
        this.redrawVertex();
    }
    // ----------------------------------------------------
    // ------------ SETTERS
    // ----------------------------------------------------
    setChosenVertex(v) {
        this.chosenVertex = v;
        if (this.chosenVertex == null) this.vertexList.forEach(vertex => (vertex.state = false));
    }

    setMouseDown(state) {
        this.mouseDown = state;
    }

    // ----------------------------------------------------
    // ------------ ADD ELEMENTS
    // ----------------------------------------------------
    addVertex(conf) {
        this.vertexList.push(new Vertex(conf));
    }

    addEdge(v1, v2) {
        Conf.Edge.v1 = v1;
        Conf.Edge.v2 = v2;
        this.edgeList.push(new Edge(Conf.Edge));
    }

    // ----------------------------------------------------
    // ------------ DELETE ELEMENTS
    // ----------------------------------------------------
    delVertex(vertex) {
        this.delVertexEdges(vertex);
        const index = this.vertexList.findIndex(v => v.x == vertex.x && v.y == vertex.y);
        this.vertexList.splice(index, 1);
    }

    delVertexEdges(vertex) {
        for (let index = this.edgeList.length - 1; index >= 0; index--) {
            if (this.edgeList[index].v1.is(vertex.x, vertex.y) || this.edgeList[index].v2.is(vertex.x, vertex.y)) this.delEdge(this.edgeList[index]);
        }
    }

    delEdge(edge) {
        const index = this.edgeList.findIndex(e => e.v1.x == edge.v1.x && e.v1.y == edge.v1.y && e.v2.x == edge.v2.x && e.v2.y == edge.v2.y);
        this.edgeList.splice(index, 1);
    }

    // ----------------------------------------------------
    // ------------ TOOLS
    // ----------------------------------------------------
    determinatePos(x, y) {
        for (const vertex of this.vertexList) if (vertex.is(x, y)) return vertex;

        for (const edge of this.edgeList) if (edge.is(x, y)) return vertex;

        return false;
    }
}

function sleep(ms) {
    console.log("WE ARE SLEAPING");
    return new Promise(resolve => setTimeout(resolve, ms));
}
