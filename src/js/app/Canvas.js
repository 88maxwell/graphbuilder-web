import Graph from "./components/Graph";
import * as Conf from "./Configs";
import { canvas, cont } from "./Consts";

export default class Canvas {
    constructor() {
        canvas.width = Conf.Graph.width;
        canvas.height = Conf.Graph.height;

        this.g = new Graph(cont);

        this.mouseDown();
        this.mouseMoveHandler = this.mouseMoveHandler.bind(this);
        this.delClickHandler = this.delClickHandler.bind(this);
    }
    //------------------------------------------
    //------------EVENTS
    //------------------------------------------
    mouseDown() {
        canvas.addEventListener("mousedown", evt => {
            this.g.setMouseDown(true);
            const { x, y } = this.getMousePos(evt);
            const obj = this.g.determinatePos(x, y);

            switch (evt.which) {
                case 1:
                    this.leftClickHandler(obj, x, y, evt.ctrlKey);
                    break;

                case 3:
                    evt.preventDefault();
                    this.rightClickHandler(obj);
                    break;

                default:
                    break;
            }
        });
    }

    mouseUp() {
        canvas.addEventListener("mouseup", evt => {
            console.log("MOUSE UP");
            canvas.removeEventListener("mousemove", this.mouseMoveHandler, false);
            this.g.setMouseDown(false);
        });
    }

    delClick() {
        canvas.addEventListener("keydown", this.delClickHandler);
    }

    mouseMove() {
        canvas.addEventListener("mousemove", this.mouseMoveHandler);
    }
    //------------------------------------------
    //------------EVENT HANDLERS
    //------------------------------------------
    
    mouseMoveHandler(evt) {
        const { x, y } = this.getMousePos(evt);
        this.g.movingVertex.changePosition(x, y);

        this.g.redrawAll();
    }

    delClickHandler(evt) {
        if (evt.key == "Delete") {
            this.g.delVertex(this.g.chosenVertex);
            this.unchoseVertex();

            this.g.redrawAll();
        }
    }

    rightClickHandler(obj) {
        console.log("rightClickHandler");
        // switch (typeof obj) {
        //     case "Vertex":
        //         if (!evt.ctrlKey) {
        //             obj.chooseVertex();
        //             this.g.changeVertexState(obj);
        //             console.log("CHOOSE VERTEX");
        //         } else this.mouseMove();
        //         break;

        //     case "Edge":
        //         break;

        //     case "boolean":
        //         this.g.addVertex(new Vertex("10", x, y, 15));
        //         break;

        //     default:
        //         break;
        // }
    }

    leftClickHandler(obj, x, y, ctrlKey) {
        switch (obj.constructor.name) {
            case "Vertex":
                if (!ctrlKey) {

                    //---------CHOOSE---------
                    if (this.g.chosenVertex == null) {
                        obj.setState(true);
                        this.g.setChosenVertex(obj);
                        this.delClick();

                    //---------ADD EDGE------------
                    } else {
                        this.g.addEdge(this.g.chosenVertex, obj);
                        this.unchoseVertex();
                        this.g.redrawVertex();
                    }

                //-------------MOVE-----------
                } else {
                    this.unchoseVertex();
                    this.g.movingVertex = obj;
                    this.mouseMove();
                    this.mouseUp();
                }
                break;

            //-------------ADD VERTEX-----------
            case "Boolean":
                this.unchoseVertex();
                Conf.Vertex.x = x;
                Conf.Vertex.y = y;
                this.g.addVertex(Conf.Vertex);
                break;

            default:
                break;
        }
    }

    //------------------------------------------
    //------------TOOLS
    //------------------------------------------
    
    unchoseVertex(){
        try {
            this.g.chosenVertex.setState(false);
            this.g.chosenVertex = null;
        } catch (error) {
            //dont have a choosen vertex
        }
    }

    getMousePos(evt) {
        const rect = canvas.getBoundingClientRect(),
            scaleX = canvas.width / rect.width,
            scaleY = canvas.height / rect.height;

        return {
            x: (evt.clientX - rect.left) * scaleX,
            y: (evt.clientY - rect.top) * scaleY
        };
    }
}
