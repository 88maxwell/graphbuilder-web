import {canvas} from "./Consts";


export let Graph = {
    width: 1300,
    height: 505,
    color: "wheat"
};

export let Vertex = {
    data: "1",
    x: 0,
    y: 0,
    r: 20,
    color: "yellow",
    chosenColor: "green",
    hoverColor: "yellow",
    highlightColor: "red",
    highlightTime: 1.5,
}

export let Edge = {
    v1: null,
    v2: null,
    width: 4,
    color: "black",
    weight: false,
    hoverColor: "yellow",
    highlightColor: "red",
    highlightTime: 1.5,
}